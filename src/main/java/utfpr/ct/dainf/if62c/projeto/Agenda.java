package utfpr.ct.dainf.if62c.projeto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;

/**
/**
 * Linguagem Java
 * @author 
 */
public class Agenda {
    private final String descricao;
    private final List<Compromisso> compromissos = new ArrayList<>();
    private final Timer timer;

    public Agenda(String descricao) {
        this.descricao = descricao;
        timer = new Timer(descricao);
    }

    public String getDescricao() {
        return descricao;
    }

    public List<Compromisso> getCompromissos() {
        return compromissos;
    }
    
    public void novoCompromisso(Compromisso compromisso) {
        compromissos.add(compromisso);
        Aviso aviso = new AvisoFinal(compromisso);
        compromisso.registraAviso(aviso);
        timer.schedule(aviso, compromisso.getData());
    }
    
    public void novoAviso(Compromisso compromisso, int antecedencia) {
        Aviso aviso = new Aviso(compromisso);
        compromisso.registraAviso(aviso);
        timer.schedule(aviso, antecedencia*1000);
    }
    
    public void novoAviso(Compromisso compromisso, int antecedencia, int intervalo) {
        Aviso aviso = new Aviso(compromisso);
        compromisso.registraAviso(aviso);
        timer.scheduleAtFixedRate(aviso, antecedencia*1000, intervalo*1000);
    }
    
    public void cancela(Compromisso compromisso) {
        compromissos.stream().filter((c1) -> (c1.equals(compromisso))).forEach((c1) -> {
            compromissos.remove(c1);
        }); 
    }
    
    public void cancela(Aviso aviso) {
        aviso.cancel();
        compromissos.remove(aviso.compromisso);
    }
    
    public void destroi() {
        compromissos.clear();
        timer.cancel();
    }
}
